console.log(`SOAL PERTAMA`)

var multiplying = 1000000
for (let i = 1; i < 8; i++) {
    console.log(i * multiplying)
    multiplying = multiplying / 10
}

console.log(`\nSOAL KEDUA`)
const data = [1, 2, 33, 44, 55, 33, 44, 22, 44, 33, 2, 77, 66, 1, 2, 3, 4, 5, 6, 7, 89, 3, 3, 8, 9, 75, 4, 3, 2, 2, 1, 3]
const panjangData = Math.ceil(data.length / 3)
var kelSatu = []
var kelDua = []
var kelTiga = []
for (let i = 0; i <= data.length - 1; i++) {
    if (i < panjangData) {
        kelSatu.push(data[i])
    } else if (i >= panjangData && i < panjangData + panjangData) {
        kelDua.push(data[i])
    } else {
        kelTiga.push(data[i])
    }
}
console.log(`Kelompok Satu ${kelSatu}`)
console.log(`Kelompok Dua ${kelDua}`)
console.log(`Kelompok Tiga ${kelTiga}`)

const hitungKelompok = (array) => {
    var jumlahKelompok = 0
    for (let i = 0; i <= array.length - 1; i++) {
        jumlahKelompok += array[i]
    }
    console.log(`Jumlah Data : ${jumlahKelompok}`)
}
hitungKelompok(kelSatu)
hitungKelompok(kelDua)
hitungKelompok(kelTiga)

const rataRataKelompok = (array) => {
    var jumlahKelompok = 0
    for (let i = 0; i <= array.length - 1; i++) {
        jumlahKelompok += array[i]
    }
    console.log(`Rata Rata : ${jumlahKelompok / array.length}`)
}
rataRataKelompok(kelSatu)
rataRataKelompok(kelDua)
rataRataKelompok(kelTiga)

const minMax = (array) => {
    var min = 9999
    var max = -1
    for (let i = 0; i < array.length; i++) {
        if (array[i] > max) {
            max = array[i]
        }
        if (array[i] < min) {
            min = array[i]
        }
    }
    console.log(`\nNilai Minimal : ${min}`)
    console.log(`Nilai Maksimal : ${max}`)
}

minMax(kelSatu)
minMax(kelDua)
minMax(kelTiga)

console.log(`\nSOAL KETIGA`)
const dataHuruf = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum mi eu magna fermentum, vel luctus tellus semper. Nunc dignissim eleifend ipsum, nec viverra mauris pellentesque non. Fusce auctor ex id mauris egestas, quis luctus nunc pharetra. Sed in dignissim nisi. Aliquam sed tempor urna, nec aliquam mi. Aenean eu feugiat lacus, vel dictum eros. Nulla condimentum porttitor aliquet. Vestibulum vehicula elit non arcu auctor maximus. Quisque est eros, maximus nec diam faucibus, mollis odio.`

const hitungAbjad = (data, kecil, kapital) => {
    var count = 0
    for (let i = 0; i < data.length; i++) {
        if (data.charAt(i) === kecil) {
            count = count + 1
        } else if (data.charAt(i) === kapital) {
            count = count + 1
        }
    }
    console.log(`karakter ${kapital} dan ${kecil} sebanyak ${count} kali`)
}

hurufKecil = "abcdefghijklmnopqrstuvwxyz"
hurufKapital = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
for (i = 0; i < hurufKecil.length; i++) {
    hitungAbjad(dataHuruf, hurufKecil.charAt(i), hurufKapital.charAt(i))
}

const geserChar = (data, kecil, kapital) => {
    var dataBaru = []
    for (let i = 0; i < data.length; i++) {
        dataBaru.push(data.charAt(i))
    }
    for (let i = 0; i < dataBaru.length; i++) {
        for (let j = 0; j < kecil.length; j++) {
            if (dataBaru[i] === kecil.charAt(j)) {
                // console.log(dataBaru[i], kecil.charAt(j))
                dataBaru[i] = kecil.charAt(j += 5)
            } else if (dataBaru[i] === kapital.charAt(j)) {
                // console.log(dataBaru[i], kapital.charAt(j))
                dataBaru[i] = kapital.charAt(j += 5)
            } else {
                dataBaru[i]
            }
        }
    }
    str = dataBaru.join("")
    console.log(str)
}

geserChar(dataHuruf, hurufKecil, hurufKapital)

console.log("\nSOAL KEEMPAT")
const tebakAngka = (angka) => {
    var count = 0
    do {
        count += 1
        console.log(`Langkah ke ${count}`)
    }
    while (angka !== Math.floor(Math.random() * 100) + 1)
    console.log(`Langkah ke ${count + 1}`)
    console.log(`Angka anda adalah ${angka}`)
    console.log(`Diselesaikan dalam ${count} langkah`)
}
var randomAngka = Math.floor(Math.random() * 100) + 1
console.log(`Angka Random adalah ${randomAngka}`)
tebakAngka(randomAngka)