console.log(`SOAL ESSAY`);
console.log(`\nJelaskan secara detail tentang bahasa pemrograman yang sedang anda kerjakan`);
console.log(`\nBahasa pemrograman adalah sebuah sintaks yang diperuntunkan untuk seorang programmer sebagai perintah
untuk menentukan apa yang akan dikerjakan oleh komputer`);

console.log(`\nJAVASCRIPT`);
console.log(`\nJavaScript merupakan bahasa yang dapat digunakan dimana saja, sebagai contoh untuk membangun aplikasi mobile maupun webs app, 
salah satu library yang menggunakan Javascript adalah React, bahasa ini juga terbilang mudah untuk dipelajari`);

console.log(`\nREACT`);
console.log(`\nREACT merupakan library yang dikembangkan oleh facebook,
React sendiri dibagi menjadi dua, React Native untuk mobile dan ReactJS web apps.`);

console.log(`\nReact Native`);
console.log(`\nReact Native adalah sebuah library front end untuk membangun
interface mobile apps. React Native bersifat cross platform untuk android dan IOS, namun hasil dari tampilan UI Android dan IOS
akan menghasilkan tampilan yang agak sedikit berbeda.`);

console.log(`\nBeberapa Aturan Javascript `);
console.log(`\n1. Penggunaan semicolon\
\tuntuk penggunaan semicolon pada javascript yang biasanya saya pakai tidak diharuskan 
\tuntuk menggunakan semicolon (kecuali pada HTML) namun untuk react native walaupun 
\ttidak menggunakan semicolon masih bisa berjalan dengan semestinya.`);
console.log(`2. Javascript bersifat Case Sensitive
\tmaksudnya ketika penulisan variable ataupun sebuah function dll menggunakan huruf besar kecil, 
\tpemanggilannya juga harus dengan penulisan yang sama`);
console.log(`3. Whitespace akan diabaikan pada baris code javascript`);
console.log(`4. Untuk membuat comment pada Javascript sama halnya seperti bahasa pemrograman pada
\tumumnya yaitu dengan menggunakan "//" atau dengan menggunakan "/*" dan diakhiri dengan  "*/" untuk multiline `)
console.log(`6. Terdapat Reserverd Word
\tReserverd Word merupakan kata-kata khusus yang dilarang untuk digunakan sebagai identifier, 
\tsebagai contoh false, else, true dan masih banyak lagi`);
console.log(`7. Pada Javascript ES6 penggunaan variable cukup menggunakan const/var/let`)

console.log(`Beberapa aturan React Native`);
console.log(`\n1. Untuk membuat component didalam react native menggunakan <View></View>`);
console.log(`\n2. Untuk penulisan teks didalam react native menggunakan <Text></Text>`);
console.log(`\n3. Untuk membuat component input didalam react native menggunakan <TextInput></TextInput>`);
console.log(`\n4. State dapat dianggap sebagai temporary storage`);
console.log(`\n5. css hanya dapat digunakan didalam StyleSheet.create({})`);
console.log(`\n6. Didalam react native sekarang tidak perlu menggunakan componentDidMount dll, sebagai gantinya dapat menggunakan Hooks`);
console.log(`\n7. Didalam react native dapat menggunakan functional component maupun class component`);
console.log(`\n8. Penggunaan library dapat diinstal menggunakan perintah npm`);

